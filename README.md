# Markdown Previewer with React.js and Marked

---

## View it [here](https://ochornb.bitbucket.io/markdown-previewer/)

![preview](https://bitbucket.org/ochornb/ochornb.bitbucket.io/raw/f340ce12a016cd7625a1616ee584bcd7a505d830/portfolio/imgs/markdown.png)

## **Tools used**

---

- HTML, CSS, JavaScript
- Bitbucket
- [marked](https://github.com/markedjs/marked)
- React.js
- Bootstrap

## **Summary**

---

I wanted to expand my front-end skill set and picked React.js to learn!

Wins:

Laying out the web page with components is fairly easy and straightforward.

Difficulties:

Trying to pass information from Parent to Child was hard to wrap my head around. I think I understand it better now.

## **Author**

---

- **Olivia Hornback** - _Front End Web Developer_ - [Website](https://ochornb.bitbucket.io/) | [LinkedIn](https://linkedin.com/in/oliviahornback)
