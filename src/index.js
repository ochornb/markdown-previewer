import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css'
import 'marked'
//import './index.css';
let marked = require('marked');

class Editor extends React.Component {
    constructor(props) {
        super(props);
        this.handleEditorTextChange = this.handleEditorTextChange.bind(this);
    }

    handleEditorTextChange(e) {
        this.props.onEditorTextChange(e.target.value);
    }
    render() {
        return (
            <div id="editor-container">
                <div className="row d-flex justify-content-center">
                    <label htmlFor="editor" className="h3">Editor</label>
                </div>

                <div className="row d-flex justify-content-center">
                    <div id="editor-background">
                        <textarea name="editor" id="editor" cols="30" rows="12" onChange={this.handleEditorTextChange} defaultValue={this.props.defaultValue}></textarea>
                    </div>

                </div>

            </div>
        );
    }


}

class Preview extends React.Component {
    previewMarkup() {
        return {
            __html: `${this.props.markdownText}`
        };
    };
    render() {
        return (
            <div >
                <div className="row d-flex justify-content-center">
                    <label htmlFor="preview" className="h3">Preview</label>

                </div>
                <div id="preview-container" className="row d-flex bg-light">
                    <div id = "preview" className ="ml-3" dangerouslySetInnerHTML={this.previewMarkup()}></div>
                </div>

            </div>
        )
    }

}

class App extends React.Component {
    constructor(props) {
        super(props);
        let placeholderText =
            `# Here's a H1\n` +
            `## Here's an H2\n ` +
            `*Emphasis* **STRONG**\n\n ` +
            `How's about a list?\n ` +
            `* [Item 1 is a Link!](#)\n ` +
            `* Item 2\n ` +
            `* Item 3\n ` +
            `### I like these dividers\n ` +
            `- - - - \n `

        this.state = {
            editorText: placeholderText,
            previewText: placeholderText,
        };

        this.handleEditorTextChange = this.handleEditorTextChange.bind(this);
    }

    handleEditorTextChange(editorText) {
        this.setState({
            editorText: editorText,
            previewText: editorText
        });
    }

    convertMarkdown(text) {
        return marked(text);
    }
    
    render() {
        return (
            <div className="container d-flex justify-content-center align-items-space-around flex-column">
                <div className="row d-flex flex-row mb-5 flex-column align-items-center">
                    <h1 className="text-align-center">Markdown Previewer</h1>
                    <hr/>
                </div>
                <div className="row d-flex align-items-stretch">
                    <div className="col col-xs-12 col-md-12 col-lg-6">
                        <Editor
                            defaultValue={this.state.editorText}
                            onEditorTextChange={this.handleEditorTextChange}
                        />
                    </div>
                    <div className="col col-xs-12 col-md-12 col-lg-6">
                        <Preview
                            markdownText={this.convertMarkdown(this.state.previewText)}
                        />

                    </div>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
